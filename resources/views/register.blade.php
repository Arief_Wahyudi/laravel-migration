@extends('master')

@section('judul')
Buat Account Baru    
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname"> <br> <br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname"> <br> <br>

    <label>Gender</label> <br>
    <input type="radio" name="wn"> Male <br>
    <input type="radio" name="wn"> Female <br> <br>

    <label>Nationality</label> <br>
    <select name="nationality" id="">
        <option value="1">Indonesia</option>
        <option value="1">Amerika</option>
        <option value="1">Inggris</option>
    </select> <br> <br>

    <label>Language Spoken:</label> <br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br> <br>

    <label>Bio</label> <br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea> <br><br>
    <input type="submit" value="Kirim">
</form>
@endsection
    