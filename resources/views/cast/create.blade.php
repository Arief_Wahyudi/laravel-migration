@extends('master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<h2>Tambah Data</h2>
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input value="{{ old('nama') }}" type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">umur</label>
        <input value="{{ old('umur') }}" type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{ old('bio') }}</textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>        
        @enderror
    </div>
        
    <button type="submit" class="btn btn-primary mt-3">Tambah</button>
</form>
   
@endsection